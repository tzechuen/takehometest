//
//  ChatMessageStringToJSONExtension.swift
//  TakeHomeTest
//
//  Created by Ting Tze Chuen on 25/11/2015.
//  Copyright © 2015 Atlassian. All rights reserved.
//

import Foundation
import SwiftHTTP

extension String {
    
    func atl_convertToJSONString(completion:(success: Bool, JSONString: String?) -> Void) {
        // Mentions
        let mentions:Array<String> = self.getMentions()
        
        // Emoticons
        let emoticons:Array<String> = self.getEmoticons()
        
        
        // Links
        self.getLinks { (success, links) -> Void in
  
            // Convert to JSON
            if success {
                var linkDictArray = Array<[String:String]>()
                
                for link in links! {
                    linkDictArray.append(link.toDictionary())
                }
                
                
                
                var resultDictionary = [String:AnyObject]()
                
                if mentions.count > 0 {
                    resultDictionary["mentions"] = mentions
                }
                
                if emoticons.count > 0 {
                    resultDictionary["emoticons"] = emoticons
                }
                
                if linkDictArray.count > 0 {
                    resultDictionary["links"] = linkDictArray
                }
                
                
                
                do {
                    let jsonData = try NSJSONSerialization.dataWithJSONObject(resultDictionary, options: .PrettyPrinted)
                    let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                    
                    completion(success: true, JSONString: jsonString)
                    
                } catch let error {
                    print("Convert to JSON Error: \(error)")
                    
                    completion(success: false, JSONString: nil)
                }

            } else {
                completion(success: false, JSONString: nil)
            }
            
        }
        
    }
    
    // MARK: Core Methods
    private func getMentions() -> Array<String> {
        return self.getObjectsFromStringWithRegex(RegularExpressions.Mentions)
    }
    
    private func getEmoticons() -> Array<String> {
        return self.getObjectsFromStringWithRegex(RegularExpressions.Emoticon)
    }
    
    private func getLinks(completion:(success: Bool, links: Array<Link>?) -> Void) {
        if let urls = self.getLinksFromString() {
            
            var links = Array<Link>()
            
            let group = dispatch_group_create()
            
            for url in urls {
                do {
                    dispatch_group_enter(group)
                    
                    let opt = try HTTP.GET(url)
                    opt.start({ (response) -> Void in
                        
                        if let responseText = response.text {                            
                            let siteTitles = responseText.getObjectsFromStringWithRegex(RegularExpressions.SiteTitle)
                            
                            links.append(Link(url: url, title: siteTitles.count > 0 ? siteTitles[0] : ""))
                            
                            dispatch_group_leave(group)
                        }
                        
                    })
                    
                } catch let error {
                    print("HTTP.GET error: \(error)")
                    
                    completion(success: false, links: nil)
                }
            }
            
            dispatch_group_notify(group, dispatch_get_main_queue(), { () -> Void in
                completion(success: true, links: links)
            })
        }
    }
    
    // MARK: Helper Methods
    private func getObjectsFromStringWithRegex(pattern: String) -> Array<String> {
        var objects = Array<String>()

        do {
            let regex = try NSRegularExpression(pattern: pattern, options: .CaseInsensitive)
            
            regex.enumerateMatchesInString(self, options: NSMatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) { (result, flags, stop) -> Void in
                if let numberOfRanges = result?.numberOfRanges {
                    var thisObject: String
                    
                    if numberOfRanges > 1 {
                        thisObject = (self as NSString).substringWithRange((result?.rangeAtIndex(1))!)
                        
                    } else {
                        thisObject = (self as NSString).substringWithRange((result?.rangeAtIndex(0))!)

                    }
                    
                    objects.append(thisObject)
                }
            }
            
            
        } catch let error {
            print("getObjectsFromStringWithRegex Error: \(error)")
            
        }
        
        return objects
    }
    
    private func getLinksFromString() -> Array<String>? {
        let types: NSTextCheckingType = [.Link]
        let detector = try? NSDataDetector(types: types.rawValue)
        
        var links = Array<String>()
        
        detector?.enumerateMatchesInString(self, options: [], range: NSMakeRange(0, self.characters.count), usingBlock: { (result, flags, stop) -> Void in
            
            if let numberOfRanges = result?.numberOfRanges {
                for index in 0..<numberOfRanges {
                    let thisLink = (self as NSString).substringWithRange((result?.rangeAtIndex(index))!)
                    
                    links.append(thisLink)
                }
            }
        })
        
        return links
    }
    
}