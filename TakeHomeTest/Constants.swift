//
//  Constants.swift
//  TakeHomeTest
//
//  Created by Ting Tze Chuen on 25/11/2015.
//  Copyright © 2015 Atlassian. All rights reserved.
//

import Foundation

struct RegularExpressions {
    static let Emoticon = "\\(([a-z0-9A-Z]{1,15})\\)"
    static let Mentions = "@(\\w+)"
    static let SiteTitle = "<title>(.*?)<\\/title>"
}
