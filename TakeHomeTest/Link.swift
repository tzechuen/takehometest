//
//  Link.swift
//  TakeHomeTest
//
//  Created by Ting Tze Chuen on 25/11/2015.
//  Copyright © 2015 Atlassian. All rights reserved.
//

import Foundation

class Link {
    
    var url: String
    var title: String
    
    init(url: String, title: String) {
        self.url = url
        self.title = title
    }
    
    func toDictionary() -> [String:String] {
        return [
            "url": url,
            "title": title
        ]
    }
}