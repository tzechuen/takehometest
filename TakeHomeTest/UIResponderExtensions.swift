//
//  UIResponderExtensions.swift
//  TakeHomeTest
//
//  Created by Ting Tze Chuen on 25/11/2015.
//  Copyright © 2015 Atlassian. All rights reserved.
//

import Foundation
import UIKit

extension UIResponder {
    
    
    func setupWithDoneToolbar() {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: "doneTapped")
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)

        toolbar.items = [flexibleSpace, doneButton]
        toolbar.sizeToFit()
        
        if self.isKindOfClass(UITextField) {
            (self as! UITextField).inputAccessoryView = toolbar
        } else if self.isKindOfClass(UISearchBar) {
            (self as! UISearchBar).inputAccessoryView = toolbar
        } else if self.isKindOfClass(UITextView) {
            (self as! UITextView).inputAccessoryView = toolbar
        }
    }
    
    func doneTapped() {
        self.resignFirstResponder()
    }
    
    
}